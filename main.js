const tasks = [
    { title: 'Faire les courses', isComplete: false },
    { title: 'Nettoyer la maison', isComplete: true },
    { title: 'Planter le jardin', isComplete: false }
  ];
  
  // Ajout d'une nouvelle tâche
    const newTask = { title: "Faire un gâteau", 
        isComplete: false };
  
  function addTask(taskList, taskToAdd) {
    let clone = [...taskList, taskToAdd]
    return clone;
  }
  console.log(addTask(tasks, newTask));
  console.log("\n");
  
  // Suppression d'une tâche
  const removeTask = (tasks, taskTitleToRemove) => {
    const updatedTasks = tasks.filter(task => task.title !== taskTitleToRemove);
    return updatedTasks;
  }
  //Exemple de supression
  const updatedTaskList = removeTask(tasks, "Nettoyer la maison");
  console.log(updatedTaskList);
  console.log("\n");
  
  // Changement d'état d'une tâche
  const toggleTaskStatus = (task) => {
    const modifiedTaskStatus = {...task, isComplete: !task.isComplete};
    return modifiedTaskStatus;
  }
  //  Exemple de changement d'état de tâche
  const taskToToggle = { 
    title:'Faire les courses', 
    isComplete: false
  };
  const toggledTask = toggleTaskStatus(taskToToggle);
  console.log(toggledTask);
  
  
  
  // Affichage de toutes les tâches
  const showAllTasks =() => {
    console.log("Toutes les tâches:")
    tasks.forEach(task => console.log(task.title));
  };
  showAllTasks();
  console.log("\n");
  
  // Affichage des tâches complétées
  const showCompletedTasks = () => {
    console.log("Tâches complétées:");
    tasks.filter(task => task.isComplete);
    tasks.forEach(task => console.log(task.title));
  };
  showCompletedTasks();
  console.log("\n");
  
  // Affichage des tâches incomplètes
  const showIncompleteTasks = () => {
    console.log("Tâches incomplètes:");
    tasks.filter(task => !task.isComplete);
    tasks.forEach(task => console.log(task.title));
  };
  showIncompleteTasks();
  console.log("\n");